"""Stream type classes for tap-crelate."""

from __future__ import annotations

import sys
import typing as t

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_crelate.client import crelateStream

if sys.version_info >= (3, 9):
    import importlib.resources as importlib_resources
else:
    import importlib_resources


class ActivitiesStream(crelateStream):
    """Define custom stream."""

    name = "activities"
    path = "/activities"
    #replication_key = "ModifiedOn"
    schema = th.PropertiesList(
        th.Property("AllDay", th.BooleanType),
        th.Property("Announced", th.BooleanType),
        th.Property("AttachmentId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("EntityName", th.StringType))),

        th.Property("Attendees", th.StringType),
        th.Property("BCC", th.StringType),
        th.Property("CC", th.StringType),
        th.Property("Completed", th.BooleanType),
        th.Property("CompletedOn", th.DateTimeType),
        th.Property("Display", th.StringType),
        th.Property("DocumentTypeId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("DueDate", th.DateTimeType),
        th.Property("FormId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("From", th.StringType),
        th.Property("Id", th.UUIDType),
        th.Property("IsCallCompleted", th.BooleanType),
        th.Property("IsEngagement", th.BooleanType),
        th.Property("IsInvite", th.BooleanType),
        th.Property("IsReachOut", th.BooleanType),
        th.Property("Location", th.StringType),
        th.Property("ModifiedOn", th.DateTimeType), 
        th.Property("Owners", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("IsPrimary", th.BooleanType))),

        th.Property("ParentExperienceId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("ParentId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("EntityName", th.StringType))),

        th.Property("Pinned", th.BooleanType),
        th.Property("Rating", th.StringType),
        th.Property("RegardingId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("Subject", th.StringType),
        th.Property("To", th.StringType),
        th.Property("VerbId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("WhatEntityIds", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("EntityName", th.StringType))),

        th.Property("When", th.DateTimeType),
        th.Property("WhenEnd", th.DateTimeType),
        th.Property("WhenOffset", th.IntegerType),
        th.Property("WhoAttendeeIds", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("EntityName", th.StringType))),

        th.Property("WorkFlowItemId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
    ).to_dict()

class ArtifactsStream(crelateStream):
    """Define custom stream."""

    name = "artifacts"
    path = "/artifacts"
    #replication_key = "ModifiedOn"
    schema = th.PropertiesList(
        th.Property("Addresses_Business", th.ObjectType(
            th.Property("City", th.StringType),
            th.Property("CountryId", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Line1", th.StringType),
            th.Property("Line2", th.StringType),
            th.Property("Location", th.ObjectType(
                th.Property("Lat", th.NumberType),
                th.Property("Lon", th.NumberType))),
            th.Property("State", th.StringType),
            th.Property("ZipCode", th.StringType))),
        th.Property("Addresses_Headquarters", th.ObjectType(
            th.Property("City", th.StringType),
            th.Property("CountryId", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Line1", th.StringType),
            th.Property("Line2", th.StringType),
            th.Property("Location", th.ObjectType(
                th.Property("Lat", th.NumberType),
                th.Property("Lon", th.NumberType))),
            th.Property("State", th.StringType),
            th.Property("ZipCode", th.StringType))),
        th.Property("Addresses_Home", th.ObjectType(
            th.Property("City", th.StringType),
            th.Property("CountryId", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Line1", th.StringType),
            th.Property("Line2", th.StringType),
            th.Property("Location", th.ObjectType(
                th.Property("Lat", th.NumberType),
                th.Property("Lon", th.NumberType))),
            th.Property("State", th.StringType),
            th.Property("ZipCode", th.StringType))),
        th.Property("Addresses_Other", th.ObjectType(
            th.Property("City", th.StringType),
            th.Property("CountryId", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Line1", th.StringType),
            th.Property("Line2", th.StringType),
            th.Property("Location", th.ObjectType(
                th.Property("Lat", th.NumberType),
                th.Property("Lon", th.NumberType))),
            th.Property("State", th.StringType),
            th.Property("ZipCode", th.StringType))),
        th.Property("ApplicationId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("BackgroundCheckProviderType", th.NumberType),
        th.Property("Class", th.StringType),
        th.Property("Code", th.StringType),
        th.Property("ContentLength", th.IntegerType),
        th.Property("ContentType", th.StringType),
        th.Property("CreatedById", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CreatedOn", th.DateTimeType),
        th.Property("DateOfBirth", th.DateTimeType),
        th.Property("Details", th.StringType),
        th.Property("EffectiveOn", th.DateTimeType),
        th.Property("EmailAddresses_Other", th.ObjectType(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("EmailFlagError", th.StringType),
            th.Property("EmailFlaggedOn", th.DateTimeType),
            th.Property("EmailFlagTypeId", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.StringType))),
        th.Property("EmailAddresses_Personal", th.ObjectType(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("EmailFlagError", th.StringType),
            th.Property("EmailFlaggedOn", th.DateTimeType),
            th.Property("EmailFlagTypeId", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.StringType))),
        th.Property("EmailAddresses_Work", th.ObjectType(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("EmailFlagError", th.StringType),
            th.Property("EmailFlaggedOn", th.DateTimeType),
            th.Property("EmailFlagTypeId", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.StringType))),
        th.Property("EthnicityId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("ExpiresOn", th.DateTimeType),
        th.Property("ExternalPrimaryKey", th.StringType),
        th.Property("FileName", th.StringType),
        th.Property("FirstName", th.StringType),
        th.Property("From", th.StringType),
        th.Property("Height", th.StringType),
        th.Property("Id", th.UUIDType),
        th.Property("IsDocument", th.BooleanType),
        th.Property("IsImage", th.BooleanType),
        th.Property("IssuedBy", th.StringType),
        th.Property("IssuedOn", th.DateTimeType),
        th.Property("IssuingState", th.StringType),
        th.Property("LastName", th.StringType),
        th.Property("MiddleName", th.StringType),
        th.Property("ModifiedOn", th.DateTimeType),
        th.Property("Name", th.StringType),
        th.Property("Owners", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("IsPrimary", th.BooleanType))),
        th.Property("PlaceOfBirth", th.StringType),
        th.Property("PrimaryArtifactIdentifier", th.StringType),
        th.Property("RevokedById", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("RevokedOn", th.DateTimeType),
        th.Property("SecondaryArtifactIdentifier", th.StringType),
        th.Property("SentOn", th.DateTimeType),
        th.Property("SexId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("Subject", th.StringType),
        th.Property("SubStatus", th.StringType),
        th.Property("SubType", th.StringType),
        th.Property("To", th.StringType),
        th.Property("TypeCode", th.StringType),
        th.Property("VerifiedOn", th.DateTimeType),
        th.Property("Weight", th.StringType),
        th.Property("WorkAuthorizationProviderType", th.NumberType),
    ).to_dict()

class EmailsStream(crelateStream):
    """Define custom stream."""

    name = "emails"
    path = "/emails"
    #replication_key = "ModifiedOn"
    schema = th.PropertiesList(
        th.Property("BCC", th.StringType),
        th.Property("CC", th.StringType),
        th.Property("CreatedById", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CreatedOn", th.DateTimeType),
        th.Property("Display", th.StringType),
        th.Property("EmailAttachmentsIds", th.ArrayType(th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("EntityName", th.StringType)))),
        th.Property("From", th.StringType),
        th.Property("Html", th.StringType),
        th.Property("Id", th.UUIDType),
        th.Property("ModifiedOn", th.DateTimeType),
        th.Property("ParentId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("EntityName", th.StringType))),
        th.Property("RegardingId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("Subject", th.StringType),
        th.Property("To", th.StringType),
        th.Property("UpdatedById", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("VerbId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("When", th.DateTimeType),
        th.Property("WhoAttendeeIds", th.ArrayType(th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("EntityName", th.StringType)))),
        th.Property("WhoBccIds", th.ArrayType(th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("EntityName", th.StringType)))),
        th.Property("WhoCcIds", th.ArrayType(th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("EntityName", th.StringType)))),
    ).to_dict()
    
class TasksStream(crelateStream):
    """Define custom stream."""

    name = "tasks"
    path = "/tasks"
    #replication_key = "ModifiedOn"
    schema = th.PropertiesList(
        th.Property("Announced", th.BooleanType),
        th.Property("CreatedBy", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("CreatedOn", th.DateTimeType),
        th.Property("CreatedOnSystem", th.DateTimeType),
        th.Property("Display", th.StringType),
        th.Property("ExeternalPrimaryKey", th.StringType),
        th.Property("Id", th.UUIDType),
        th.Property("ModifiedOn", th.DateTimeType),
        th.Property("Owners", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("IsPrimary", th.BooleanType))),

        th.Property("ParentId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("EntityName", th.StringType))),

        th.Property("Pinned", th.BooleanType),
        th.Property("RegardingId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("When", th.DateTimeType),
    ).to_dict()

class UsersStream(crelateStream):
    """Define custom stream."""

    name = "users"
    path = "/users"
    #replication_key = "ModifiedOn"
    schema = th.PropertiesList(
        th.Property("Color", th.StringType),
        th.Property("ContactId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("CreatedOn", th.DateTimeType),
        th.Property("Email", th.StringType),
        th.Property("FirstName", th.StringType),
        th.Property("FullName", th.StringType),
        th.Property("IconAttachmentId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("Id", th.UUIDType),
        th.Property("Initials", th.StringType),
        th.Property("LastName", th.StringType),
        th.Property("ModifiedOn", th.StringType),
        th.Property("UserStateId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("UserTypeId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
    ).to_dict()

class PaymentsStream(crelateStream):
    name = "payments"
    path = "/payments"
    #replication_key = "ModifiedOn"
    schema = th.PropertiesList(
        th.Property("AccountId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("Amount", th.NumberType),
        th.Property("CreatedById", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("CreatedOn", th.DateTimeType),
        th.Property("ExternalId", th.StringType),
        th.Property("Id", th.UUIDType),
        th.Property("ModifiedOn", th.DateTimeType),
        th.Property("PaymentMethodOption", th.IntegerType),
        th.Property("PaymentNum", th.IntegerType),
        th.Property("PaymentTypeOption", th.IntegerType),
        th.Property("ReceivedOn", th.DateTimeType),
    ).to_dict()

class InvoicesStream(crelateStream):
    name = "invoices"
    path = "/invoices"
    #replication_key = "ModifiedOn"
    schema = th.PropertiesList(
        th.Property("AccountId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("Balance", th.IntegerType),
        th.Property("BalanceChangedOn", th.DateTimeType),
        th.Property("CreatedById", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("CreatedOn", th.DateTimeType),
        th.Property("Credits", th.IntegerType),
        th.Property("Discounts", th.IntegerType),
        th.Property("DueOn", th.DateTimeType),
        th.Property("EmployeeId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("EntitiyStatus", th.IntegerType),
        th.Property("ExternalInvoiceId", th.StringType),
        th.Property("ExternalInvoiceUrl", th.StringType),
        th.Property("FirstSentById", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),  
        
        th.Property("FirstSentOn", th.DateTimeType),
        th.Property("HasError", th.BooleanType),
        th.Property("Id", th.UUIDType),
        th.Property("InvoiceDate", th.DateTimeType),
        th.Property("InvoiceNumber", th.IntegerType),
        th.Property("IsLate", th.BooleanType),
        th.Property("IsSentToExternal", th.BooleanType),
        th.Property("JobId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        
        th.Property("LastSentById", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("LastSentOn", th.DateTimeType),
        th.Property("ModifiedOn", th.DateTimeType),
        th.Property("Name", th.StringType),
        th.Property("Payments", th.IntegerType),
        th.Property("PrimaryDocumentAttachmentId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("SendToExternalById", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("SendToExternalOn", th.DateTimeType),
        th.Property("Subtotal", th.IntegerType),
        th.Property("Taxes", th.IntegerType),
        th.Property("TermsOption", th.IntegerType),
        th.Property("Total", th.IntegerType),
        th.Property("VoidDate", th.DateTimeType),
        th.Property("VoidedById", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("VoidedOn", th.DateTimeType),
        th.Property("WasSent", th.BooleanType),
        th.Property("WriteOffs", th.IntegerType),
    ).to_dict()

class PlacementsStream(crelateStream):
    name = "placements"
    path = "/placements"
    #replication_key = "ModifiedOn"
    schema = th.PropertiesList(
        th.Property("AccountId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("AdditionalDetails", th.StringType),
        th.Property("Addresses_Business", th.ObjectType(
            th.Property("City", th.StringType),
            th.Property("CountryId", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Line1", th.StringType),
            th.Property("Line2", th.StringType),
            th.Property("Location", th.ObjectType(
                th.Property("Lat", th.NumberType),
                th.Property("Lon", th.NumberType))),
                
            th.Property("State", th.StringType),
            th.Property("ZipCode", th.StringType))),

        th.Property("BillDate", th.DateTimeType),
        th.Property("BillRate", th.NumberType),
        th.Property("Bonus", th.NumberType),
        th.Property("BurdenRate", th.NumberType),
        th.Property("BurdenRatePercentage", th.NumberType),
        th.Property("candidateSourceId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("CreatedOn", th.DateTimeType),
        th.Property("CustomField1", th.StringType),
        th.Property("CustomField10", th.StringType),
        th.Property("CustomField11", th.NumberType),
        th.Property("CustomField12", th.NumberType),
        th.Property("CustomField13", th.NumberType),
        th.Property("CustomField14", th.NumberType),
        th.Property("CustomField15", th.NumberType),
        th.Property("CustomField16", th.NumberType),
        th.Property("CustomField17", th.NumberType),
        th.Property("CustomField18", th.NumberType),
        th.Property("CustomField19", th.NumberType),
        th.Property("CustomField2", th.StringType),
        th.Property("CustomField20", th.NumberType),
        th.Property("CustomField21", th.DateTimeType),
        th.Property("CustomField22", th.DateTimeType),
        th.Property("CustomField23", th.DateTimeType),
        th.Property("CustomField24", th.DateTimeType),
        th.Property("CustomField25", th.DateTimeType),
        th.Property("CustomField26", th.DateTimeType),
        th.Property("CustomField27", th.DateTimeType),
        th.Property("CustomField28", th.DateTimeType),
        th.Property("CustomField29", th.DateTimeType),
        th.Property("CustomField3", th.StringType),
        th.Property("CustomField30", th.DateTimeType),
        th.Property("CustomField31", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField32", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField33", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField34", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField35", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField36", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField37", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField38", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField39", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField4", th.StringType),
        th.Property("CustomField40", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField41", th.StringType),
        th.Property("CustomField42", th.StringType),
        th.Property("CustomField43", th.StringType),
        th.Property("CustomField44", th.StringType),
        th.Property("CustomField45", th.StringType),
        th.Property("CustomField46", th.StringType),
        th.Property("CustomField47", th.StringType),
        th.Property("CustomField48", th.StringType),
        th.Property("CustomField49", th.StringType),
        th.Property("CustomField5", th.NumberType),
        th.Property("CustomField50", th.StringType),
        th.Property("CustomField51", th.NumberType),
        th.Property("CustomField52", th.NumberType),
        th.Property("CustomField53", th.NumberType),
        th.Property("CustomField54", th.NumberType),
        th.Property("CustomField55", th.NumberType),
        th.Property("CustomField56", th.NumberType),
        th.Property("CustomField57", th.NumberType),
        th.Property("CustomField58", th.NumberType),
        th.Property("CustomField59", th.NumberType),
        th.Property("CustomField6", th.StringType),
        th.Property("CustomField60", th.NumberType),
        th.Property("CustomField61", th.DateTimeType),
        th.Property("CustomField62", th.DateTimeType),
        th.Property("CustomField63", th.DateTimeType),
        th.Property("CustomField64", th.DateTimeType),
        th.Property("CustomField65", th.DateTimeType),
        th.Property("CustomField66", th.DateTimeType),
        th.Property("CustomField67", th.DateTimeType),
        th.Property("CustomField68", th.DateTimeType),
        th.Property("CustomField69", th.DateTimeType),
        th.Property("CustomField7", th.StringType),
        th.Property("CustomField70", th.DateTimeType),
        th.Property("CustomField71", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField72", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField73", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField74", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField75", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField76", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField77", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField78", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField79", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField8", th.StringType),
        th.Property("CustomField80", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField9", th.StringType),
        th.Property("Discount", th.NumberType),
        th.Property("Duration", th.NumberType),
        th.Property("EndDate", th.DateTimeType),
        th.Property("EndingReason", th.IntegerType),
        th.Property("EndingReasonDescription", th.StringType),
        th.Property("EntitiyStatus", th.IntegerType),
        th.Property("EstimatedEndDate", th.DateTimeType),
        th.Property("ExperienceId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("Fee", th.NumberType),
        th.Property("FeePercent", th.NumberType),
        th.Property("HourlyRate", th.NumberType),
        th.Property("Id", th.UUIDType),
        th.Property("JobOpenDate", th.DateTimeType),
        th.Property("JobSchedule", th.StringType),
        th.Property("JobTtleId", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

        th.Property("LeadSourceId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("Margin", th.NumberType),
        th.Property("ModifiedOn", th.DateTimeType),
        th.Property("Name", th.StringType),
        th.Property("OpprotunityTypeId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("OTBillRate", th.NumberType),
        th.Property("OTBillRateMultiplier", th.NumberType),
        th.Property("OTPayRate", th.NumberType),
        th.Property("OTPayRateMultiplier", th.NumberType),
        th.Property("Owners", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("IsPrimary", th.BooleanType))),

        th.Property("PayRate", th.NumberType),
        th.Property("PayRateType", th.NumberType),
        th.Property("PlacementNum", th.IntegerType),
        th.Property("Salary", th.NumberType),
        th.Property("Shift", th.IntegerType),
        th.Property("StartDate", th.DateTimeType),
        th.Property("StatusReason", th.StringType),
        th.Property("TimeToFill", th.NumberType),
        th.Property("TotalHours", th.NumberType),
        th.Property("UserId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("value", th.NumberType),
        th.Property("VerbId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("WarrantyDate", th.DateTimeType),
        th.Property("When", th.DateTimeType),
    ).to_dict()

class ApplicationsStream(crelateStream):
    name = "applications"
    path = "/applications"
    #replication_key = "ModifiedOn"
    schema = th.PropertiesList(
        th.Property("Addresses_Business", th.ObjectType(
            th.Property("City", th.StringType),
            th.Property("CountryId", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Line1", th.StringType),
            th.Property("Line2", th.StringType),
            th.Property("Location", th.ObjectType(
                th.Property("Lat", th.NumberType),
                th.Property("Lon", th.NumberType))),

            th.Property("State", th.StringType),
            th.Property("ZipCode", th.StringType))),
        
        th.Property("Addressess_Home", th.ObjectType(
            th.Property("City", th.StringType),
            th.Property("CountryId", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Line1", th.StringType),
            th.Property("Line2", th.StringType),
            th.Property("Location", th.ObjectType(
                th.Property("Lat", th.NumberType),
                th.Property("Lon", th.NumberType))),

            th.Property("State", th.StringType),
            th.Property("ZipCode", th.StringType))),

        th.Property("Addresses_Other", th.ObjectType(
            th.Property("City", th.StringType),
            th.Property("CountryId", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Line1", th.StringType),
            th.Property("Line2", th.StringType),
            th.Property("Location", th.ObjectType(
                th.Property("Lat", th.NumberType),
                th.Property("Lon", th.NumberType))),

            th.Property("State", th.StringType),
            th.Property("ZipCode", th.StringType))),

        th.Property("ContactId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("ContactSourceId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("CreatedById", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("CreatedOn", th.DateTimeType),

        th.Property("CurrentCompanyId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("Education", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.StringType),
            th.Property("ValueIds", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("WhatId", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType),
                th.Property("EnittyName", th.StringType))),

            th.Property("WhatValue", th.StringType),
            th.Property("WhenEnd", th.DateTimeType),
            th.Property("WhenStart", th.DateTimeType),
            th.Property("WhereId", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType),
                th.Property("EnittyName", th.StringType))))),

        th.Property("EmailAddresses_Other", th.ObjectType(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.StringType))),

        th.Property("EmailAddresses_Personal", th.ObjectType(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.StringType))),

        th.Property("EmailAddresses_Work", th.ObjectType(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.StringType))),

        th.Property("ExternalPrimaryKey", th.StringType),
        th.Property("FirstName", th.StringType),
        th.Property("FormId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("Id", th.UUIDType),
        th.Property("JobId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("JobTitle", th.StringType),
        th.Property("LastName", th.StringType),
        th.Property("MiddleName", th.StringType),
        th.Property("ModifiedOn", th.DateTimeType),
        th.Property("Name", th.StringType),
        th.Property("NickName", th.StringType),
        th.Property("Owners", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("IsPrimary", th.BooleanType))),

        th.Property("ParsedCity", th.StringType),
        th.Property("ParsedEmail", th.StringType),
        th.Property("ParsedFirstName", th.StringType),
        th.Property("ParsedLastName", th.StringType),
        th.Property("ParsedPhone", th.StringType),
        th.Property("ParsedState", th.StringType),
        th.Property("ParsedZip", th.StringType),
        th.Property("PhoneNumbers_Fax", th.ObjectType(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Extension", th.StringType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("PhoneFlagTypeId", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("Value", th.StringType))),

        th.Property("PhoneNumbers_Home", th.ObjectType(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Extension", th.StringType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("PhoneFlagTypeId", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("Value", th.StringType))),

        th.Property("PhoneNumbers_Mobile", th.ObjectType(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Extension", th.StringType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("PhoneFlagTypeId", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("Value", th.StringType))),

        th.Property("PhoneNumbers_Other", th.ObjectType(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Extension", th.StringType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("PhoneFlagTypeId", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("Value", th.StringType))),

        th.Property("PhoneNumbers_Skype", th.ObjectType(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Extension", th.StringType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("PhoneFlagTypeId", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("Value", th.StringType))),

        th.Property("PhoneNumbers_Work_Direct", th.ObjectType(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Extension", th.StringType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("PhoneFlagTypeId", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("Value", th.StringType))),

        th.Property("PhoneNumbers_Work_Main", th.ObjectType(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Extension", th.StringType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("PhoneFlagTypeId", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("Value", th.StringType))),

        th.Property("RawHttpReferer", th.StringType),
        th.Property("SourceId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("StatusId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("Tags", th.ObjectType(
            th.Property("property1", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("property2", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))))),

        th.Property("Websites_Blog", th.ObjectType(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.StringType))),

        th.Property("Websites_Business", th.ObjectType(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.StringType))),

        th.Property("Websites_Facebook", th.ObjectType(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.StringType))),

        th.Property("Websites_LinkedIn", th.ObjectType(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.StringType))),

        th.Property("Websites_Other", th.ObjectType(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.StringType))),

        th.Property("Websites_Personal", th.ObjectType(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.StringType))),
    ).to_dict()

class JobStream(crelateStream):
    name = "jobs"
    path = "/jobs"
    #replication_key = "ModifiedOn"
    schema = th.PropertiesList(
        th.Property("AccountId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
        )),
        th.Property("ActualValue", th.NumberType),
        th.Property("AdditionalContacts_Billing", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("EntityName", th.StringType),
            th.Property("Value", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType),
                th.Property("EntityName", th.StringType),
            )),
        )),
        th.Property("AdditionalContacts_ExecutiveSponsor", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType),
                th.Property("EntityName", th.StringType),
            )),
        )),
        th.Property("AdditionalContacts_HiringManager", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("EntityName", th.StringType),
            th.Property("Value", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType),
                th.Property("EntityName", th.StringType),
            )),
        )),
        th.Property("AdditionalContacts_InternalRecruiter", th.ObjectType(
           th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("EntityName", th.StringType),
            th.Property("Value", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType),
                th.Property("EntityName", th.StringType),
            )),
        )),
        th.Property("AdditionalContacts_OtherContact", th.ObjectType(
           th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("EntityName", th.StringType),
            th.Property("Value", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType),
                th.Property("EntityName", th.StringType),
            )),
        )),
        th.Property("AdditionalContacts_Sales_ClientContact", th.ObjectType(
           th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("EntityName", th.StringType),
            th.Property("Value", th.ObjectType(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType),
                th.Property("EntityName", th.StringType),
            )),
        )),
        th.Property("AvailableDeliveryTypes", th.ArrayType(th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("EntityName", th.StringType),
        ))),
        th.Property("BillRate", th.NumberType),
        th.Property("Bonus", th.NumberType),
        th.Property("BurdenRate", th.NumberType),
        th.Property("BurdenRatePercentage", th.NumberType),
        th.Property("ClosedOn", th.DateTimeType),
        th.Property("Commission", th.NumberType),
        th.Property("Compensation", th.NumberType),
        th.Property("CompensationDetails", th.StringType),
        th.Property("CustomField1", th.StringType),
        th.Property("CustomField10", th.StringType),
        th.Property("CustomField11", th.NumberType),
        th.Property("CustomField12", th.NumberType),
        th.Property("CustomField13", th.NumberType),
        th.Property("CustomField14", th.NumberType),
        th.Property("CustomField15", th.NumberType),
        th.Property("CustomField16", th.NumberType),
        th.Property("CustomField17", th.NumberType),
        th.Property("CustomField18", th.NumberType),
        th.Property("CustomField19", th.NumberType),
        th.Property("CustomField2", th.StringType),
        th.Property("CustomField20", th.NumberType),
        th.Property("CustomField21", th.DateTimeType),
        th.Property("CustomField22", th.DateTimeType),
        th.Property("CustomField23", th.DateTimeType),
        th.Property("CustomField24", th.DateTimeType),
        th.Property("CustomField25", th.DateTimeType),
        th.Property("CustomField26", th.DateTimeType),
        th.Property("CustomField27", th.DateTimeType),
        th.Property("CustomField28", th.DateTimeType),
        th.Property("CustomField29", th.DateTimeType),
        th.Property("CustomField3", th.StringType),
        th.Property("CustomField30", th.DateTimeType),
        th.Property("CustomField31", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField32", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField33", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField34", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField35", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField36", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField37", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField38", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField39", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField4", th.StringType),
        th.Property("CustomField40", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField41", th.StringType),
        th.Property("CustomField42", th.StringType),
        th.Property("CustomField43", th.StringType),
        th.Property("CustomField44", th.StringType),
        th.Property("CustomField45", th.StringType),
        th.Property("CustomField46", th.StringType),
        th.Property("CustomField47", th.StringType),
        th.Property("CustomField48", th.StringType),
        th.Property("CustomField49", th.StringType),
        th.Property("CustomField5", th.NumberType),
        th.Property("CustomField50", th.StringType),
        th.Property("CustomField51", th.NumberType),
        th.Property("CustomField52", th.NumberType),
        th.Property("CustomField53", th.NumberType),
        th.Property("CustomField54", th.NumberType),
        th.Property("CustomField55", th.NumberType),
        th.Property("CustomField56", th.NumberType),
        th.Property("CustomField57", th.NumberType),
        th.Property("CustomField58", th.NumberType),
        th.Property("CustomField59", th.NumberType),
        th.Property("CustomField6", th.StringType),
        th.Property("CustomField60", th.NumberType),
        th.Property("CustomField61", th.DateTimeType),
        th.Property("CustomField62", th.DateTimeType),
        th.Property("CustomField63", th.DateTimeType),
        th.Property("CustomField64", th.DateTimeType),
        th.Property("CustomField65", th.DateTimeType),
        th.Property("CustomField66", th.DateTimeType),
        th.Property("CustomField67", th.DateTimeType),
        th.Property("CustomField68", th.DateTimeType),
        th.Property("CustomField69", th.DateTimeType),
        th.Property("CustomField7", th.StringType),
        th.Property("CustomField70", th.DateTimeType),
        th.Property("CustomField71", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField72", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField73", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField74", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField75", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField76", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField77", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField78", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField79", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField8", th.StringType),
        th.Property("CustomField80", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField9", th.StringType),
        th.Property("Description", th.StringType),
        th.Property("Discount", th.NumberType),
        th.Property("Duration", th.NumberType),
        th.Property("EmploymentSearchContactId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("EstimatedCloseDate", th.DateTimeType),
        th.Property("EstimatedEndDate", th.DateTimeType),
        th.Property("ExpectedValue", th.NumberType),
        th.Property("Fee", th.NumberType),
        th.Property('FeePercent', th.NumberType),
        th.Property("HiringManagerId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("HourlyRate", th.NumberType),
        th.Property("Id", th.UUIDType),
        th.Property("IsFeatured", th.BooleanType),
        th.Property("IsHidden", th.BooleanType),
        th.Property("IsLead", th.BooleanType),
        th.Property("IsOnHold", th.BooleanType),
        th.Property("IsPublishedToFreeBoards", th.BooleanType),
        th.Property("JobCode", th.StringType),
        th.Property("JobNum", th.NumberType),
        th.Property("JobSchedule", th.StringType),
        th.Property("JobTitleId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("LastActionDate", th.DateTimeType),
        th.Property("LastActivityDate", th.DateTimeType),
        th.Property("LastActivityOrModifiedOn", th.DateTimeType),
        th.Property("LastEngagementDate", th.DateTimeType),
        th.Property("LastReachOutDate", th.DateTimeType),
        th.Property("LeadSourceId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("LostReasonTypeId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("Margin", th.NumberType),
        th.Property("ModifiedOn", th.DateTimeType),
        th.Property("Name", th.StringType),
        th.Property("NumberOfOpenings", th.NumberType),
        th.Property("NumberOfPlacements", th.NumberType),
        th.Property("OTBillRate", th.NumberType),
        th.Property("OTBillRateMultiplier", th.NumberType),
        th.Property("OTPayRate", th.NumberType),
        th.Property("OTPayRateMultiplier", th.NumberType),
        th.Property("Owners", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("IsPrimary", th.BooleanType))),
        th.Property("PayRate", th.NumberType),
        th.Property("PayRateType", th.NumberType),
        th.Property("PlacementStatus", th.NumberType),
        th.Property("PortalCity", th.StringType),
        th.Property("PortalCompanyname", th.StringType),
        th.Property("PortalCompensation", th.NumberType),
        th.Property("PortalDescription", th.StringType),
        th.Property("PortalState", th.StringType),
        th.Property("PortalTitle", th.StringType),
        th.Property("PortalUrlSlug", th.StringType),
        th.Property("PortalZip", th.StringType),
        th.Property("PotentialValue", th.NumberType),
        th.Property("Probability", th.NumberType),
        th.Property("Salary", th.NumberType),
        th.Property("Shift", th.NumberType),
        th.Property("TimeEntryFormat", th.NumberType),
        th.Property("TimeToClose", th.NumberType),
        th.Property("TotalHours", th.NumberType),
    ).to_dict()

class LossesStream(crelateStream):
    name = "losses"
    path = "/losses"
    #replication_key = "ModifiedOn"
    schema = th.PropertiesList(
        th.Property("CompetitorId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("CreatedOn", th.DateTimeType),
        th.Property("CustomField1", th.StringType),
        th.Property("CustomField10", th.StringType),
        th.Property("CustomField11", th.NumberType),
        th.Property("CustomField12", th.NumberType),
        th.Property("CustomField13", th.NumberType),
        th.Property("CustomField14", th.NumberType),
        th.Property("CustomField15", th.NumberType),
        th.Property("CustomField16", th.NumberType),
        th.Property("CustomField17", th.NumberType),
        th.Property("CustomField18", th.NumberType),
        th.Property("CustomField19", th.NumberType),
        th.Property("CustomField2", th.StringType),
        th.Property("CustomField20", th.NumberType),
        th.Property("CustomField21", th.DateTimeType),
        th.Property("CustomField22", th.DateTimeType),
        th.Property("CustomField23", th.DateTimeType),
        th.Property("CustomField24", th.DateTimeType),
        th.Property("CustomField25", th.DateTimeType),
        th.Property("CustomField26", th.DateTimeType),
        th.Property("CustomField27", th.DateTimeType),
        th.Property("CustomField28", th.DateTimeType),
        th.Property("CustomField29", th.DateTimeType),
        th.Property("CustomField3", th.StringType),
        th.Property("CustomField30", th.DateTimeType),
        th.Property("CustomField31", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField32", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField33", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField34", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField35", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField36", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField37", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField38", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField39", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField4", th.StringType),
        th.Property("CustomField40", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField41", th.StringType),
        th.Property("CustomField42", th.StringType),
        th.Property("CustomField43", th.StringType),
        th.Property("CustomField44", th.StringType),
        th.Property("CustomField45", th.StringType),
        th.Property("CustomField46", th.StringType),
        th.Property("CustomField47", th.StringType),
        th.Property("CustomField48", th.StringType),
        th.Property("CustomField49", th.StringType),
        th.Property("CustomField5", th.NumberType),
        th.Property("CustomField50", th.StringType),
        th.Property("CustomField51", th.NumberType),
        th.Property("CustomField52", th.NumberType),
        th.Property("CustomField53", th.NumberType),
        th.Property("CustomField54", th.NumberType),
        th.Property("CustomField55", th.NumberType),
        th.Property("CustomField56", th.NumberType),
        th.Property("CustomField57", th.NumberType),
        th.Property("CustomField58", th.NumberType),
        th.Property("CustomField59", th.NumberType),
        th.Property("CustomField6", th.StringType),
        th.Property("CustomField60", th.NumberType),
        th.Property("CustomField61", th.DateTimeType),
        th.Property("CustomField62", th.DateTimeType),
        th.Property("CustomField63", th.DateTimeType),
        th.Property("CustomField64", th.DateTimeType),
        th.Property("CustomField65", th.DateTimeType),
        th.Property("CustomField66", th.DateTimeType),
        th.Property("CustomField67", th.DateTimeType),
        th.Property("CustomField68", th.DateTimeType),
        th.Property("CustomField69", th.DateTimeType),
        th.Property("CustomField7", th.StringType),
        th.Property("CustomField70", th.DateTimeType),
        th.Property("CustomField71", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField72", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField73", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField74", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField75", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField76", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField77", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField78", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField79", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField8", th.StringType),
        th.Property("CustomField80", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("CustomField9", th.StringType),

        th.Property("Id", th.UUIDType),
        th.Property("JobId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("LossReasonTypeId", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("ModifiedOn", th.DateTimeType),
        th.Property("When", th.DateTimeType),
    ).to_dict()

class OpportunitiesStream(crelateStream):
    name = "opportunityTypes"
    path = "/opportunityTypes"
    #replication_key = "ModifiedOn"
    schema = th.PropertiesList(
        th.Property("CollectionOftypeIds", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("Color", th.StringType),
        th.Property("DefaultExpansetypeIds", th.ObjectType(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("DefaultTimeEntryFormat", th.NumberType),
        th.Property("Description", th.StringType),
        th.Property("Icon", th.StringType),
        th.Property("Id", th.UUIDType),
        th.Property("IsChildOnly", th.BooleanType),
        th.Property("IsCollectionType", th.BooleanType),
        th.Property("IsDefault", th.BooleanType),
        th.Property("IsDeleted", th.BooleanType),
        th.Property("IsEnabled", th.BooleanType),
        th.Property("Name", th.StringType),
        th.Property("PluralName", th.StringType),
        th.Property("RecruitingType", th.NumberType),
        th.Property("SortOrder", th.NumberType),
        th.Property("ValueMode", th.NumberType),
    ).to_dict()