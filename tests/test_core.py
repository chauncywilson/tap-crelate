"""Tests standard tap features using the built-in SDK tests library."""

import datetime

from singer_sdk.testing import get_tap_test_class

from tap_crelate.tap import Tapcrelate

SAMPLE_CONFIG = {
    "start_date": datetime.datetime.now(datetime.timezone.utc).strftime("%Y-%m-%d"),
    "api_key": "1234567890",
    "api_url": "https://sandbox.crelate.com/api3"
}


# Run standard built-in tap tests from the SDK:
TestTapcrelate = get_tap_test_class(
    tap_class=Tapcrelate,
    config=SAMPLE_CONFIG,
)


# TODO: Create additional tests as appropriate for your tap.
